import argparse
import colordialog
import uploader
import colorcorrect

parser = argparse.ArgumentParser(description = "Handle images in different ways.")

parser.add_argument("-color-correct", action = "store_const", dest = "color_correct",
					help = "Correct an image's colors to simulate a type of color blindness",
					default = False, const = True)

parser.add_argument("-share", action = "store_const", dest = "share",
					help = "Share an image to imgur/Twitter",
					default = False, const = True)

parser.add_argument("-palette", action = "store", dest = "palette",
					help = "Generate a color palette and dominant color from an image. Relative or absolute path to file.", default = False)

result = parser.parse_args()

if (result.palette):
	print("This function can be very slow depending on image size. Please allow around 30 seconds.")
	colordialog.analyzeImage(result.palette)
elif (result.color_correct):
	colorcorrect.colorCorrect()
elif (result.share):
	uploader.share()