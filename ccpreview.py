from tkinter import *
from tkinter.ttk import *
from tkinter import filedialog
from PIL import ImageTk, Image
import urllib.parse

class beforeAndAfter(Frame):
    global root
    root = Tk()

    def __init__(self, parent):
        Frame.__init__(self, parent)

        self.parent = parent
        self.initUI()


    def initUI(self):

        self.parent.title("Buttons")
        self.style = Style()
        self.style.theme_use("default")

        frame = Frame(self, relief=RAISED, borderwidth=1)
        frame.pack(fill=BOTH, expand=True)

        path  = "AA.jpg"    #path to the first image
        path2 = "zz.jpg"    #path to the second Image
        img = ImageTk.PhotoImage(Image.open(path))  #image file
        img2 = ImageTk.PhotoImage(Image.open(path2))    #image file #2
        img.image = img     #anchor both references to the images
        img2.image = img2
        panel = Label(root, image = img)    #create both panels for the images
        panelTwo = Label(root, image = img2)
        panel.pack(side = "top", fill = "both", expand = "yes") #add them to the window
        panelTwo.pack(side = "top", fill  = "both", expand  = "yes")

        self.pack(fill=BOTH, expand=True)

        closeButton = Button(self, text="Close", command = buttonClose)
        closeButton.pack(side= BOTTOM, padx=5, pady=5)

def buttonClose():
    root.destroy()  #close the window

def ccpreview():
    root.geometry("300x200+300+300")
    app = beforeAndAfter(root)
    root.mainloop()