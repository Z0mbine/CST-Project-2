from PIL import Image, ImageTk
from lib.colorthief import ColorThief
import tkinter as tk
from tkinter.ttk import *
from tkinter.messagebox import *
from os.path import isfile
import struct

# Let's make some variables
dominantColorSize = (230, 70)
colorIconSize = (170, 70)
iconPadding = 3

def label(master, x, y, w, h, *args, **kwargs):
	f = Frame(master, height = h, width = w)
	f.pack_propagate(0)  # don't shrink
	f.place(x = x, y = y)
	label = Label(f, *args, **kwargs)
	label.pack(fill = 'both', expand = 1)
	return label

def tohex(rgb):
	return '#%02x%02x%02x' % rgb

def GetSize(panel):
	return tuple(int(var) for var in panel.geometry().split('+')[0].split('x'))

def GetWidth(panel):
	return GetSize(panel)[0]

def GetHeight(panel):
	return GetSize(panel)[1]

def Center(panel):
	panel.update_idletasks()
	w = panel.winfo_screenwidth()
	h = panel.winfo_screenheight()
	size = GetSize(panel)
	x = w / 2 - size[0] / 2
	y = h / 2 - size[1] / 2

	panel.geometry("%dx%d+%d+%d" % (size + (x, y)))

def analyzeImage(path, colorCount = 6):
	if (not isfile(path)):
		showerror("Error", "An unexpected error occured!\nThe selected file does not exist!")
		return

	thiefObject = ColorThief(path)
	imgWidth = thiefObject.image.width
	imgHeight = thiefObject.image.height
	panelSize = (1700, 1060)

	colorBarWidth = (colorIconSize[0] * colorCount) + (iconPadding * colorCount)

	palette = thiefObject.get_palette(color_count = colorCount)
	dominant = thiefObject.get_color(quality = 1)

	PANEL = tk.Tk()
	PANEL.title("Color Palette")
	PANEL.geometry("%sx%s" % panelSize)

	Center(PANEL)

	imgContainer = Frame(PANEL, width = panelSize[0], height = panelSize[1] / 1.5)
	imgContainer.pack()

	img = thiefObject.image
	img.thumbnail((panelSize[0], panelSize[1] / 1.5), Image.ANTIALIAS)

	preview = ImageTk.PhotoImage(img)

	previewImage = Label(imgContainer, image = preview)
	previewImage.pack()

	colorDock = Frame(PANEL, width = panelSize[0], height = panelSize[1] - (panelSize[1] / 1.5))
	colorDock.pack(fill = "both", anchor = 'center')

	dominantColor = label(colorDock, panelSize[0] / 2 - dominantColorSize[0] / 2, 0, dominantColorSize[0], dominantColorSize[1], foreground = "white", text = str(dominant), background = tohex(dominant))
	dominantColor.config(font = ("Courier", 18))

	colorIndex = 0

	for color in palette:
		paletteColor = label(colorDock, (panelSize[0] / 2 - colorBarWidth / 2) + (colorIndex * (colorIconSize[0] + iconPadding)), 100, colorIconSize[0], colorIconSize[1], foreground = "white", text = str(color), background = tohex(color))
		paletteColor.config(font = ("Courier", 11))
		colorIndex += 1

	PANEL.mainloop()