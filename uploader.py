from tkinter import *
from tkinter.ttk import *
from tkinter import filedialog
from PIL import Image
import urllib.parse
import lib.pyimgur as pyimgur


class Example(Frame):

    def __init__(self, parent):
        Frame.__init__(self, parent)

        self.parent = parent
        self.initUI()


    def initUI(self):

        self.parent.title("Buttons")
        self.style = Style()
        self.style.theme_use("default")

        frame = Frame(self, relief=RAISED, borderwidth=1)
        frame.pack(fill=BOTH, expand=True)

        self.pack(fill=BOTH, expand=True)

        closeButton = Button(self, text="Close")
        closeButton.pack(side=RIGHT, padx=5, pady=5)
        uploadButton = Button(self, text = "Save/Upload", command  = buttonClick)
        uploadButton.pack(side = RIGHT)

def buttonClick():
    CLIENT_ID = "48b9a282f14d299"   #ClienID for legally uploading to imgur
    im1 = pyimgur.Imgur(CLIENT_ID)  #Ready an image to be uploaded to imgur
    filename  = filedialog.askopenfilename()    #Choose the file to upload
    uploaded_image = im1.upload_image(filename, title = "Uploaded with pyImgur") #upload the image to imgur
    print(uploaded_image.title) #Prints the Image title to the command line "Uploaded with pyImgur"
    print("Heres Your Link Below: ")
    print(uploaded_image.link)  #Prints the link to the image on imgur
    return

def share():
    root = Tk()
    root.geometry("300x200+300+300")
    app = Example(root)
    root.mainloop()