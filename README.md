# CST Project 2

This project aims to create a multi-faceted program which allows users to perform various operations on images.

# Features

## Colorblindness Simulator

Ever wondered what an image looks like through your colorblind friend's eyes? Now's your chance!

## Color Palette Generator

Find an image whose color scheme interests you, and the program will generate a small color palette for you to use!

## Sharing

Like an image? Upload it to imgur!

# Instructions

## Dependencies

This project assumes you have numpy and Pillow installed, and are using Python 3.<br>
Other minor dependencies are included.

## Generating a color palette

Navigate to the folder where you have downloaded and extracted this project.<br>
Once there, open a command window.<br>
To generate your palette, type:<br>
`python main.py -palette "path/to/file"`<br>
A window will pop up shortly after with your palette, as below!<br>
![Preview of Palette](http://zombine.me/ss/python-14.10.16247.png)

## Simulating colorblindness

Similar to the above, open a command window in the project directory.<br>
Drag your favorite image into the project folder, and rename it to `1a.jpg` (your file must be a JPEG)<br>
This time, enter the command:<br>
`python main.py -color-correct`<br>
Follow the instructions that follow, and you'll receive something like below!<br>
![Preview of Colorblind Sim](http://zombine.me/ss/python-14.10.16246.png)

## Sharing to imgur

Yet again, the same as the above steps, but the command is:<br>
`python main.py -share`<br>
This will create a small dialog.<br><br>

![Preview of Dialog](http://zombine.me/ss/python-14.10.16245.png)<br><br>

Click `Save/Upload` to open a file dialog. Find an image you'd like to upload and click `Ok`
